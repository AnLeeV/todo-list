import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const storeData = {
	state: {
		isShow: true,
		show: false,
		listTodos: [
			// { id: 1, task: 'task 1', level: 'high', isChecked: true },
			// { id: 2, task: 'task 2', level: 'Small', isChecked: false },
			// { id: 3, task: 'task 3', level: 'high', isChecked: true },
		],
		id: 0,
		task: '',
		level: ''
	},
	getters: {

	},
	actions: {
		deleteTodo({ commit }, todoId){
			commit('DEL_EVENT', todoId)
		},
		addTodo({ commit }, newTask) {
			commit('ADD_EVENT', newTask)
		}
	},
	mutations: {
		TOGGLE_SHOW(state){
			state.isShow = !state.isShow
		},
		TOGGLE_SHOW_SORT(state) {
			state.show = !state.show
		},
		DEL_EVENT(state, todoId) {
			var deleteID = -1;
			if(todoId.id === state.listTodos.id){
				deleteID = todoId
			}
			if(confirm('chắc không bạn trẻ???')){
				state.listTodos.splice(deleteID, 1)
			}

		},
		ADD_EVENT(state, newTask) {
			state.listTodos.unshift(newTask)
		}
	},


}

const store = new Vuex.Store(storeData)

export default store